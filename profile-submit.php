<?php
session_start();
require 'database.php';

if(!(isset($_POST['name']))){
  echo "You did not fill one or more text fields, returning to creation page.<br>";
  echo "<a href='create-profile.html'> Return</a>";
}

$filename = basename($_FILES['my-file']['name']);
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
  echo "Invalid filename";
  exit;
}

// // Get the username and make sure it is valid
// $username = $_SESSION['username'];
// if( !preg_match('/^[\w_\-]+$/', $username) ){
//   echo "Invalid username";
//   exit;
// }

//create full path
$full_path = sprintf("/Applications/MAMP/htdocs/ica2/uploads/%s", $filename);

//put item into database
// http://classes.engineering.wustl.edu/cse330/index.php/PHP_and_MySQL
$name = $_POST['name'];
$email = $_POST['email'];
$description = $_POST['description'];
$age = (int)$_POST['age'];

$pic_path = sprintf("/ica2/uploads/%s", $filename);


$stmt = $mysqli->prepare("insert into users (name, email, description, age, pictureUrl) values (?, ?, ?, ?, ?)");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}

$stmt->bind_param('sssis', $name, $email, $description, $age, $pic_path);

$stmt->execute();

$stmt->close();
//move the uploaded file into the proper location

if( move_uploaded_file($_FILES['my-file']['tmp_name'], $full_path) ){
  header("Location: show-users.php");
  exit;
}
else{
  echo "There was an error with your upload";
}
?>
