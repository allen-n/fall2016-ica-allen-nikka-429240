<!DOCTYPE html>
<head>
  <meta charset="utf-8"/>
  <title>Matches</title>
  <style type="text/css">
  body{
    width: 760px; /* how wide to make your web page */
    background-color: teal; /* what color to make the background */
    margin: 0 auto;
    padding: 0;
    font:12px/16px Verdana, sans-serif; /* default font */
  }
  div#main{
    background-color: #FFF;
    margin: 0;
    padding: 10px;
  }
  </style>
</head>
<body><div id="main">
  <h1>All Users</h1>
  <p>
    <form name="age_select" action= "age-range.php" method="POST" enctype='multipart/form-data'>
      <p>
        <br><label for="age">High end of Age Range:</label>
        <input type="number" name="high_age" min="18"/><br>
        <br><label for="age">Low end of Age Range:</label>
        <input type="number" name="low_age" min="18"/><br>
      </p>
      <p>
        <input type="submit" value="Submit"/>
      </p>
    </form>
  </p>
  <?php
  require 'database.php';

  echo "<a href='create-profile.html'>Make another profile</a>";
  $stmt = $mysqli->prepare("select name, email, age, description, pictureUrl from users order by id");
  if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
  }

  $stmt->execute();

  $stmt->bind_result($name, $email, $age, $description, $fullpath);

  echo "<ul>\n";
  while($stmt->fetch()){
    printf("\t<li>Name: %s<br>Email: %s <img src='%s' alt='img not found' height='240' width='300'/></li>\n
    Age: %s</li>\nDescription: %s</li>\n",
    htmlspecialchars($name),
    htmlspecialchars($email),
    htmlspecialchars($fullpath),
    htmlspecialchars($age),
    htmlspecialchars($description)
  );
}

echo "</ul>\n";

$stmt->close();
?>
</div></body>
</html>
