<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Matchmaking Site - Users by Age</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
  <h1>Users in Age Rang</h1>
<?php
require 'database.php';

$ageH = (int)$_POST['high_age'];
$ageL = (int)$_POST['low_age'];
echo "<a href='create-profile.html'>Make another profile</a>";
$stmt = $mysqli->prepare("SELECT name, email, age, description, pictureUrl from users WHERE age < ? AND age > ?");
if(!$stmt){
  printf("Query Prep Failed: %s\n", $mysqli->error);
  exit;
}
$stmt->bind_param('ii', $ageH, $ageL);

$stmt->execute();

$stmt->bind_result($name, $email, $age, $description, $fullpath);

echo "<ul>\n";
while($stmt->fetch()){
  printf("\t<li>Name: %s<br>Email: %s <img src='%s' alt='img not found' height='240' width='300'/></li>\n
  Age: %s</li>\nDescription: %s</li>\n",
  htmlspecialchars($name),
  htmlspecialchars($email),
  htmlspecialchars($fullpath),
  htmlspecialchars($age),
  htmlspecialchars($description)
);
}

echo "</ul>\n";

$stmt->close();

 ?>
<!-- CONTENT HERE -->

</div></body>
</html>
